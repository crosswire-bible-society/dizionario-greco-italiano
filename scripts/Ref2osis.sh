#!/bin/sh
for FILE in *.xml ; do
# modtype
# modtype=TEI (<ref osisRef="..)
# modtype=OSIS (<reference osisRef="..)
MODTYPE='TEI'

# debug > 0 -> debug msgs displayed on /dev/stderr
DEBUG="0"

# Versification
# v11n=kjv
# v11n=calvin
# v11n=catholic
# v11n=lxx
V11N='Catholic'

# run example
cat $FILE | \
 awk -v modtype="$MODTYPE" -v debug="$DEBUG" -v v11n="$V11N" '
 @include "ref2osis.awk"
 {
  print ref2osis($0)
 }
 ' > $FILE.ref
rename 's/xml.ref/ref.xml/g' *.ref
 done


